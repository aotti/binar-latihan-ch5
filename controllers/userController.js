const { User } = require('../models/index')

class UserController {
    createUser(reqb) {
        return User.create({
            name: reqb.name,
            address: reqb.address,
            age: reqb.age,
            job: reqb.job,
            city: reqb.city,
            email: reqb.email
        })
    }

    getAllUser() {
        return User.findAll()
    }
}

module.exports = UserController