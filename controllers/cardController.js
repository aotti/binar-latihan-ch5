const { Card } = require('../models/index')

class CardController {
    createCard(reqb) {
        return Card.create({
            card_number: reqb.cardNumber,
            issuer: reqb.issuer,
            is_status: reqb.isStatus,
            UserId: reqb.UserId
        })
    }

    getAllCard() {
        return Card.findAll()
    }
}

module.exports = CardController