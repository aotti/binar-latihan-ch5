const express = require('express')
const app = express()
const port = 3000
const userRoute = require('./routes/userRoute')
const cardRoute = require('./routes/cardRoute')

// UNTUK METHOD GET AGAR DATA YG TAMPIL JADI JSON
app.use(express.json());
// UNTUK METHOD POST AGAR BISA AMBIL VALUE DARI INPUT FORM
app.use(express.urlencoded({ extended: true }));

app.get('/', (req, res)=>{
    res.send('selamat datang')
})
app.use('/user', userRoute)
app.use('/card', cardRoute)

app.listen(port, ()=>{console.log(`listening to port ${port}`);})