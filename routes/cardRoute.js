
const express = require('express');
const router = express.Router();
const getCardController = require('../controllers/cardController')
const cardController = new getCardController()

router.use((req, res, next) => {
    console.log(`${req.method} ${req.url}`);
    next();
});

router.get('/check', (req, res)=>{
    res.status(200).send(`masuk route card`)
})

router.post('/create', (req, res)=>{
    cardController.createCard(req.body).then(result => {
        res.status(200).send(result)
    })
    .catch(err => { res.status(500).send(err) })
})

router.get('/show', (req, res)=>{
    cardController.getAllCard().then(result => {
        res.status(200).send(result)
    })
    .catch(err => { res.status(500).send(err) })
})


module.exports = router