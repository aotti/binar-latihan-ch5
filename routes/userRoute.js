
const express = require('express');
const router = express.Router();
const getUserController = require('../controllers/userController')
const userController = new getUserController()

router.use((req, res, next) => {
    console.log(`${req.method} ${req.url}`);
    next();
});

router.get('/check', (req, res)=>{
    res.status(200).send(`masuk route user`)
})

router.post('/create', (req, res)=>{
    if(req.body.email.includes('@')) {
        userController.createUser(req.body).then(result => {
            res.status(200).send(result)
        })
        .catch(err => { res.status(500).send(err) })
    }
    else {
        res.status(200).send(`format email salah`)
    }
})

router.get('/show', (req, res)=>{
    userController.getAllUser().then(result => {
        res.status(200).send(result)
    })
    .catch(err => { res.status(500).send(err) })
})


module.exports = router