'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.bulkInsert('Users', [{
      name: 'John Doe',
      address: 'Jl. Pahlawan',
      age: '22',
      job: 'Supir',
      city: 'Jakarta',
      email: 'john@gmail.com',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      name: 'Wawan',
      address: 'Jl. Sehat',
      age: '17',
      job: 'Pelajar',
      city: 'Jakarta',
      email: 'wawan@gmail.com',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      name: 'Gilang',
      address: 'Jl. Permanen',
      age: '33',
      job: 'Kuproy',
      city: 'Bengkulu',
      email: 'gilang@yahoo.com',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      name: 'Imam',
      address: 'Jl. Kemarin',
      age: '12',
      job: 'Pelajar',
      city: 'Medan',
      email: 'imam@yahoo.com',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      name: 'Lele',
      address: 'Jl. Kolam',
      age: '21',
      job: 'Supir',
      city: 'Bengkulu',
      email: 'lele@gmail.com',
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Users', null, {});
  }
};
